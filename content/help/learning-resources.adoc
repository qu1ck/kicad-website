+++
title = "Learning Resources"
summary = "Community created resources to learn KiCad"
aliases = [ "/help/tutorials/", "/help/books" ]
[menu.main]
    parent = "Help"
    name   = "Learning Resources"
    weight = 30
+++
:toc: macro
:toc-title:

toc::[]

== Books
=== KiCad Like a Pro

"KiCad Like a Pro, 3rd Edition" written by Dr. Peter Dalmaris from
https://techexplorations.com[Tech Explorations] is great reference for
KiCad users of all skill levels.  This book covers everything from getting
started using KiCad to using git for project version control.  A
https://techexplorations.com/so/kicad-like-a-pro-3e-special-edition/[special edition] of this book has been created of which half
of the profits from each sale will be donated to the CERN & Society
Foundation for KiCad development.


== Tutorials

Several text-based tutorials as well as video tutorials have been put together by various KiCad users. Here you can find a limited subset.

=== English

==== Textual Tutorials

- link:/help/documentation/#_getting_started[KiCad Getting Started Tutorial]
- https://forum.kicad.info/t/faq-index-thread/8890[User Forum FAQ]
- https://kicad.mmccoo.com/kicad-scripting-table-of-contents/[KiCad Scripting Examples]
- https://www.woolseyworkshop.com/2019/07/01/performing-a-circuit-simulation-in-kicad/[Performing A Circuit Simulation In KiCad]
- http://happyrobotlabs.com/posts/tutorials/tutorial-3d-kicad-parts-using-openscad-and-wings3d/[Happy Robot Labs: 3D KiCad Parts Using OpenSCAD and Wings3D]
- https://thequantizer.com/2021/03/01/dac-and-pwm-kicad-simulation/[The Quantizer: DAC and PWM KiCad Simulation]

==== Video Tutorials

- https://www.youtube.com/user/contextualelectronic/playlists[Videos by Chris Gammell] at Contextual Electronics (7 parts ~2.5 hours)
- https://www.youtube.com/watch?v=4XiKCgU5s20[DAC and PWM KiCad Simulation]

=== French

==== Video Tutorials

- https://www.youtube.com/playlist?list=PLuQznwVAhY2WA4CIf3_aB_e8hRCSbQuUp[KiCad 5.1 video tutorials by Eric Peronnin]
